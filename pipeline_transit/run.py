import json
import time

import requests
import csv
import os
import socket

HEX_EXTRACT_PATH = "/data/hex_tiles_dump.csv"
HEX_OUT_PATH = "/data/hex_tiles_data.csv"


PLACES = [
    [51.50449, -0.08647]
]

"""
, # The Shard
    [51.51293, -0.12213], # Covent Garden Theatre
    [51.49942, -0.16334], # Harrods
    [51.51125, -0.08692], # Monument (subway station in London City)
    [51.51599, -0.17490]  # Paddington (near Hyde Park)
"""

MAX_MINUTES = 60*5


def get_eta_minutes(json_response):
    if "paths" not in json_response:
        return MAX_MINUTES
    
    for route in json_response["paths"]:
        if "time" not in route:
            print("No time in route")
            return MAX_MINUTES
        time_ms = route["time"]
        
        return time_ms / (1000.0*60.0)
    
    return MAX_MINUTES


def calc_eta(lat, lon, transit_endpoint):
    avg_eta_minutes = 0.0

    for place in PLACES:
        # https://github.com/graphhopper/graphhopper/blob/master/docs/web/api-doc.md
        payload = {
            'type': 'json', 
            'locale': 'en-US',
            'key': '',
            'pt.earliest_departure_time': '2022-02-05T17:34:00.000Z',
            'pt.limit_solutions': 1,
            'elevation': False,
            'instructions': False,
            'calc_points': False,
            'profile': 'pt',
            'point': [','.join([str(lat), str(lon)]), ','.join([str(place[0]), str(place[1])])]
        }

        resp = requests.get(transit_endpoint, params=payload)

        eta_minutes = get_eta_minutes(resp.json())
        avg_eta_minutes += eta_minutes

    return avg_eta_minutes/len(PLACES)


def wait_for_graphhopper(transit_endpoint):
    while True:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex(('urbasaur_transit_navigation', 8989))
        sock.close()
        if result == 0:
            print("Graphhopper is ready")
            return
        
        print("Waiting for Graphhopper...")
        time.sleep(30)
        

def main():
    print("Started calculating transit for hexes")
    transit_endpoint = os.getenv("TRANSIT_NAVIGATION_URL", "")
    print(f"Transit endpoint: {transit_endpoint}")
    wait_for_graphhopper(transit_endpoint)

    with open(HEX_EXTRACT_PATH, 'r') as f_in:
        with open(HEX_OUT_PATH, 'w') as f_out:
            reader = csv.reader(f_in, delimiter=',', quotechar='"')
            writer = csv.writer(f_out, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

            for i, row in enumerate(reader):
                print(f"Handling {i} hex")
                gid, center_json = row
                center = json.loads(center_json)
                avg_eta_minutes = calc_eta(center["coordinates"][1], center["coordinates"][0], transit_endpoint)
                writer.writerow([gid, avg_eta_minutes])
    
    print("Done")


if __name__ == "__main__":
    main()