import json
import logging
import os
import shutil
import zipfile

import geopandas
import psycopg2
import requests
from shapely.geometry import Polygon

logger = logging.getLogger('import_geojson_to_maps_db')
logger.setLevel(logging.INFO)
# create file handler which logs even debug messages
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(ch)

GEOJSON_DATA = [
    {
        "raw_file": "ne_10m_geography_marine_polys/ne_10m_geography_marine_polys.shp",
        "borders_file": "oceans_clipped.geojson",
        "table": "world_oceans",
        "source_url": "https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/physical/ne_10m_ocean.zip",
        "need_to_create_subfolder": True
    },

    {
        "raw_file": "ne_10m_populated_places/ne_10m_populated_places.shp",
        "borders_file": "places_clipped.geojson",
        "table": "world_cities",
        "source_url": "https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/cultural/ne_10m_populated_places.zip",
        "need_to_create_subfolder": True
    },
    {
        "raw_file": "ne_10m_admin_0_countries/ne_10m_admin_0_countries.shp",
        "borders_file": "countries_clipped.geojson",
        "table": "world_countries",
        "source_url": "https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/10m/cultural/ne_10m_admin_0_countries.zip",
        "need_to_create_subfolder": True
    },
    {
        "raw_file": "water-polygons-split-4326/water_polygons.shp",
        "borders_file": "oceans_detailed_clipped.geojson",
        "table": "world_oceans_detailed",
        "source_url": "https://osmdata.openstreetmap.de/download/water-polygons-split-4326.zip",
        "need_to_create_subfolder": False
    }]


"""

"""

SCALE = 0
EMPTY_VAL = ""

CRS_WGS84 = {"type": "name", "properties": {"name": "EPSG:4326"}}


def get_filename_from_url(url):
    assert url.find("/")
    return url.rsplit("/", 1)[1]


def download_file_from_url(url, data_path: str):
    filename = get_filename_from_url(url)
    logger.info(f"Fetching {url}")
    try:
        resp = requests.get(url, allow_redirects=True, stream=True, headers={"User-Agent": "Urbasaur spider"})
    except Exception as e:
        logger.exception(f"Could not load {url}")
        raise

    if resp.status_code != 200:
        msg = f"Error {resp.status_code} fetching {url}"
        logger.error(msg)
        raise Exception(msg)

    dst_file = os.path.join(data_path, filename)
    with open(dst_file, 'wb') as f:
        resp.raw.decode_content = True
        shutil.copyfileobj(resp.raw, f)

    return dst_file


def extract_archive(src_archive, dst_path):
    with zipfile.ZipFile(src_archive, "r") as zip_ref:
        zip_ref.extractall(dst_path)


def prepare_files(data, data_path: str):
    filepath = data["raw_file"]
    if os.path.isfile(os.path.join(data_path, filepath)):
        logger.info(f"{filepath} exists")
        return

    path, filename = filepath.split("/")
    zip_path = download_file_from_url(data["source_url"], data_path)
    logger.info(f"Downloaded {zip_path}")
    if data["need_to_create_subfolder"]:
        extract_archive(zip_path, os.path.join(data_path, path))
    else:
        extract_archive(zip_path, data_path)

    logger.info(f"Extracted {zip_path}")


def fill_global_tables(database_url: str, data_path: str):
    conn = psycopg2.connect(database_url)

    cursor = conn.cursor()

    for d in GEOJSON_DATA:
        logger.info(f"Handling {d['table']}")
        prepare_files(d, data_path)

        shp_file = geopandas.read_file(os.path.join(data_path, d["raw_file"]))
        logger.info("Parsed shape file")
        if d["table"] == "world_oceans_detailed":
            polygon = Polygon(
                [(-180.0, 0), (-180.0, 89.0), (180.0, 89.0), (180.0, 0)]
            )
        else:
            polygon = Polygon(
                [
                    (-180.0, -89.0),
                    (-180.0, 89.0),
                    (180.0, 89.0),
                    (180.0, -89.0),
                ]
            )

        world_clipped = geopandas.clip(shp_file, polygon)
        logger.info("Clipped")
        filepath = os.path.join(data_path, d["borders_file"])
        world_clipped.to_file(filepath, driver="GeoJSON")
        logger.info("Saved to file")

        with open(filepath) as f:
            data = json.load(f)

            for obj in data["features"]:

                if "geometry" not in obj or obj["geometry"] is None:
                    logger.warning("No geometry. Skip.")
                    continue

                props = dict(
                    (k.lower(), v) for k, v in obj["properties"].items()
                )
                scalerank = int(props.get("min_label", SCALE))

                stype = props.get("featurecla", EMPTY_VAL)

                name = EMPTY_VAL
                if "name" in props and props["name"] is not None:
                    name = props["name"]
                    name = name.replace("'", " ")


                obj["geometry"]["crs"] = CRS_WGS84
                geo_json = json.dumps(obj["geometry"])
                table = d["table"]
                cursor.execute(
                    f"INSERT INTO {table} (scalerank, type, name, geometry) "
                    f"VALUES ({scalerank}, '{stype}', '{name}', "
                    f"ST_TRANSFORM(ST_MakeValid(ST_GeomFromGeoJSON('{geo_json}')), 3857))"
                )
        logger.info("Finished handling")

    conn.commit()


def run_stage(database_url: str, data_path: str) -> None:
    fill_global_tables(database_url, data_path)
