from pathlib import Path

import pipeline.stages.seed_tiles.seed_tiles as tiles
import multiprocessing


def run_stage(database_url: str, osm_dir: Path, root_tiles_path: Path):
    threads = 5 # max(multiprocessing.cpu_count(), 8)/2
    print(f"Before seeding tiles in {threads} threads")
    tiles.seed_tiles(database_url, osm_dir, root_tiles_path, threads_count=threads)
