# -*- coding: utf-8 -*-

THREADS_COUNT = 2

# Directory for saving tiles:

default_source = "osm"

sources = [
    {
        "name": "global",
        # Bounding for tiles consisting of 2 points. Southwest and Northeast
        # World boundaries.
        # if you set abs(lat) to more then 85 degrees, you will have negative y.
        "bbox_p1_deg": [-85.0, -180.0],  # lat, lon
        "bbox_p2_deg": [85.0, 180.0],

        "def_min_zoom": 0,
        "def_max_zoom": 7,
        # 0, 1, 2, 3, 4, 5
        "zoom_levels": [0, 1, 2, 3, 4, 5, 6, 7]
    },
    {
        "name": "analytics",

        "def_min_zoom": 6,
        "def_max_zoom": 15,

        "zoom_levels": [6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    },
    {
        "name": default_source,

        "def_min_zoom": 6,
        "def_max_zoom": 15,

        "zoom_levels": [6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    }
]

supported_cities = "'London' "

small_towns = "'Scientific station', 'Historic place', 'Meteorogical Station', 'Populated place'"

railway_stations = "'London Waterloo', 'London Paddington', 'London Kings Cross', 'London St. Pancras International', " \
    "'London Euston', 'Charing Cross', 'London Victoria', 'London Bridge', 'London Fenchurch Street'"

switch_for_building_type = "CASE WHEN type in ('industrial', 'garages', 'warehouse', 'service', " \
                           "'manufacture', 'hangar') THEN 'i' " \
                           "WHEN type in ('house', 'residential', 'apartments', 'detached', 'terrace', " \
                           "'semidetached_house', 'cabin', 'bungalow', 'semi', 'yes') THEN 'l' ELSE  'c' END as type"

# Layers of vector tiles:
layers = [
    {
        "layer_name": "ecology_poi",
        "table": "ecology_poi",
        "fields": "poi_type, title as name, "
                  "operator, url, start_year, energy_source, poi_subtype, owner",
        "source": "analytics",
        "additional_where": "and poi_type <> 'substation'"
    },
    {
        "layer_name": "infrastructure_poi",
        "table": "infrastructure_poi",
        "fields": "poi_type",
        "source": "analytics",
        "min_zoom": 6,
        "max_zoom": 7,
        "additional_where": "and min_level <= 6"
    },
    {
        "layer_name": "infrastructure_poi",
        "table": "infrastructure_poi",
        "fields": "poi_type",
        "source": "analytics",
        "min_zoom": 8,
        "max_zoom": 9,
        "additional_where": "and min_level <= 8"
    },
    {
        "layer_name": "infrastructure_poi",
        "table": "infrastructure_poi",
        "fields": "poi_type",
        "source": "analytics",
        "min_zoom": 10,
        "max_zoom": 11,
        "additional_where": "and min_level <= 9"
    },
    {
        "layer_name": "infrastructure_poi",
        "table": "infrastructure_poi",
        "fields": "poi_type",
        "source": "analytics",
        "min_zoom": 12,
        "max_zoom": 12,
        "additional_where": "and min_level <= 11"
    },
    {
        "layer_name": "infrastructure_poi",
        "table": "infrastructure_poi",
        "fields": "poi_type",
        "source": "analytics",
        "min_zoom": 13,
        "max_zoom": 13,
        "additional_where": "and min_level <= 12"
    },
    {
        "layer_name": "infrastructure_poi",
        "table": "infrastructure_poi",
        "fields": "poi_type",
        "source": "analytics",
        "min_zoom": 14,
        "max_zoom": 14
    },
    {
        "layer_name": "infrastructure_poi",
        "table": "infrastructure_poi",
        "fields": "poi_type, title as name",
        "source": "analytics",
        "min_zoom": 15
    },
    {
        "layer_name": "hex",
        "table": "hex_tiles_xl",
        "fields": "impact, transport_accessibility, "
                  "infrastructure::numeric::integer as infrastructure,"
                  "filter_subway, filter_park, filter_where_to_eat, filter_school, filter_kindergarten, filter_shop",
        "min_zoom": 6,
        "max_zoom": 8,
        "source": "analytics"
    },
    {
        "layer_name": "hex",
        "table": "hex_tiles_l",
        "fields": "impact, transport_accessibility, "
                  "infrastructure::numeric::integer as infrastructure,"
                  "filter_subway, filter_park, filter_where_to_eat, filter_school, filter_kindergarten, filter_shop",
        "min_zoom": 9,
        "max_zoom": 9,
        "source": "analytics"
    },
    {
        "layer_name": "hex",
        "table": "hex_tiles_m",
        "fields": "impact, transport_accessibility, "
                  "infrastructure::numeric::integer as infrastructure,"
                  "filter_subway, filter_park, filter_where_to_eat, filter_school, filter_kindergarten, filter_shop",
        "min_zoom": 10,
        "max_zoom": 10,
        "source": "analytics"
    },
    {
        "layer_name": "hex",
        "table": "hex_tiles_s",
        "fields": "impact, transport_accessibility, "
                  "infrastructure::numeric::integer as infrastructure,"
                  "filter_subway, filter_park, filter_where_to_eat, filter_school, filter_kindergarten, filter_shop",
        "min_zoom": 11,
        "max_zoom": 11,
        "source": "analytics"
    },
    {
        "layer_name": "hex",
        "table": "hex_tiles",
        "fields": "impact, transport_accessibility, "
                  "infrastructure::numeric::integer as infrastructure,"
                  "filter_subway, filter_park, filter_where_to_eat, filter_school, filter_kindergarten, filter_shop",
        "min_zoom": 12,
        "source": "analytics"
    },
    {
        "layer_name": "world_water_poly",
        "table": "world_oceans",
        "fields": "",
        "mode_clip": False,
        "max_zoom": 5,
        "source": "global"
    },
    {
        "layer_name": "world_water_poly",
        "table": "world_oceans_detailed",
        "fields": "",
        "mode_clip": False,
        "min_zoom": 6,
        "max_zoom": 7,
        "source": "global"
    },
    {
        "layer_name": "world_countries_poly",
        "table": "world_countries",
        "fields": "",
        "mode_clip": False,
        "max_zoom": 5,
        "source": "global"
    },
    {
        "layer_name": "world_countries_names",
        "table": "world_countries",
        "fields": "name",
        "mode_clip": False,
        "source": "global",
        "geometry_operator": "centroid",
        "additional_where": "and scalerank < ",
        "append_zoom_to_additional_where": True
    },
    {
        "layer_name": "target_cities",
        "table": "world_cities",
        "fields": "name",
        "mode_clip": False,
        "max_zoom": 5,
        "source": "global",
        "additional_where": f"and name in ({supported_cities})"
    },
    {
        "layer_name": "world_cities",
        "table": "world_cities",
        "fields": "name, CASE "
                  "WHEN type = 'Admin-0 capital' THEN 'bb' "
                  "WHEN type = 'Admin-1 capital' THEN 'b' "
                  "ELSE  'c' "
                  "END as city_type ",
        "mode_clip": False,
        "max_zoom": 5,
        "source": "global",
        "additional_where": f"and name not in ({supported_cities}) and type not in ({small_towns}) and scalerank <= ",
        "append_zoom_to_additional_where": True
    },
    {
        "layer_name": "buildings_parts",
        "table": "osm_building_parts_poly",
        "fields": f"{switch_for_building_type}, "
                  f"get_height_m(\"building_levels\", \"building_height\") as h, "
                  f" infrastructure::numeric::integer as infrastructure",
        "min_zoom": 15,
        "max_zoom": 15,
        "additional_where": "and is_drawable = 1"
    },
    {
        "layer_name": "buildings",
        "table": "osm_buildings_poly",
        "fields": f"id, {switch_for_building_type}, "
                  "get_height_m(\"building_levels\", \"building_height\") as h, housenumber as n,"
                  " infrastructure::numeric::integer as infrastructure",
        "min_zoom": 15,
        "max_zoom": 15,
        "additional_where": "and is_drawable is null"
    },
    # Road sizes are used in styles.json to render road width.
    # We match road type to its size.
    {
        "layer_name": "roads",
        "table": "osm_roads_gen0",
        "fields": "type",
        "min_zoom": 6,
        "max_zoom": 7
    },
    {
        "layer_name": "roads",
        "table": "osm_roads_gen1",
        "fields": "type",
        "min_zoom": 8,
        "max_zoom": 9,
    },
    {
        "layer_name": "roads",
        "table": "osm_roads_gen2",
        "fields": "type, "
                  "CASE WHEN short_name <> '' THEN short_name ELSE name END as name",
        "min_zoom": 10,
        "max_zoom": 10
    },
    {
        "layer_name": "roads",
        "table": "osm_roads_gen2",
        "fields": "type, "
                  "CASE WHEN short_name <> '' THEN short_name ELSE name END as name",
        "min_zoom": 11,
        "max_zoom": 11
    },
    {
        "layer_name": "roads",
        "table": "osm_roads_gen3",
        "fields": "type, "
                  "CASE WHEN short_name <> '' THEN short_name ELSE name END as name",
        "min_zoom": 12,
        "max_zoom": 12
    },
    {
        "layer_name": "roads",
        "table": "osm_roads_gen4",
        "fields": "type, "
                  "CASE WHEN short_name <> '' THEN short_name ELSE name END as name",
        "min_zoom": 13,
        "max_zoom": 13
    },
    {
        "layer_name": "roads",
        "table": "osm_roads_gen5",
        "fields": "type, name",
        "min_zoom": 14,
        "max_zoom": 14
    },
    {
        "layer_name": "roads",
        "table": "osm_roads_way",
        "fields": "type, name",
        "min_zoom": 15
    },
    {
        "layer_name": "road_refs",
        "table": "osm_roads_gen1",
        "fields": "DISTINCT ON (ref) ref",
        "min_zoom": 8,
        "max_zoom": 8,
        "additional_where": "and ref in ('M1', 'M3', 'M4', 'M20', 'M25')",
        "geometry_operator": "point_on_surface",
    },
    {
        "layer_name": "road_refs",
        "table": "osm_roads_gen1",
        "fields": "DISTINCT ON (ref) ref",
        "min_zoom": 9,
        "max_zoom": 9,
        "additional_where": "and ref in ('M1', 'M3', 'M4', 'M20', 'M25', 'M2', 'M23', 'M11', 'M40', 'A1', 'A1(M)')",
        "geometry_operator": "point_on_surface",
    },
    {
        "layer_name": "road_refs",
        "table": "osm_roads_gen2",
        "fields": "DISTINCT ON (ref) ref",
        "min_zoom": 10,
        "max_zoom": 11,
        "additional_where": "and ref <> '' and lanes > 3",
        "geometry_operator": "point_on_surface",
    },
    {
        "layer_name": "footways",
        "table": "osm_footways_way",
        "fields": "",
        "min_zoom": 14,
        "max_zoom": 14,
        "additional_where": "and type = 'pedestrian'"
    },
    {
        "layer_name": "footways",
        "table": "osm_footways_way",
        "fields": "",
        "min_zoom": 15
    },
    {
        "layer_name": "railways",
        "table": "osm_railways_gen0",
        "fields": "",
        "min_zoom": 6,
        "max_zoom": 9,
        "additional_where": "and type not in ('monorail', 'tram')"
    },
    {
        "layer_name": "railways",
        "table": "osm_railways_gen1",
        "fields": "",
        "min_zoom": 10,
        "max_zoom": 11,
        "additional_where": "and type not in ('monorail', 'tram')"
    },
    {
        "layer_name": "railways",
        "table": "osm_railways_gen2",
        "fields": "",
        "min_zoom": 12,
        "max_zoom": 12,
        "additional_where": "and type not in ('monorail', 'tram')"
    },
    {
        "layer_name": "railways",
        "table": "osm_railways_way",
        "fields": "",
        "min_zoom": 13,
        "additional_where": "and type not in ('monorail', 'tram')"
    },
    {
        "layer_name": "trams",
        "table": "osm_railways_gen2",
        "fields": "",
        "min_zoom": 12,
        "max_zoom": 12,
        "additional_where": "and type in ('monorail', 'tram')"
    },
    {
        "layer_name": "trams",
        "table": "osm_railways_way",
        "fields": "",
        "min_zoom": 13,
        "additional_where": "and type in ('monorail', 'tram')"
    },
    {
        "layer_name": "osm_subway_route_members",
        "table": "osm_subway_route_members",
        "fields": "colour",
        "additional_where": "and handled=1",
        "min_zoom": 6
    },
    {
        "layer_name": "residential",
        "table": "osm_residential_zones",
        "fields": "name",
        "min_zoom": 14
    },
    {
        "layer_name": "natural",
        "table": "osm_natural_gen0",
        "fields": "",
        "min_zoom": 6,
        "max_zoom": 8
    },
    {
        "layer_name": "natural_name",
        "table": "osm_natural_gen0",
        "fields": "name",
        "min_zoom": 6,
        "max_zoom": 8,
        "geometry_operator": "point_on_surface",
        "additional_where": f"and name <> ''"
    },
    {
        "layer_name": "natural",
        "table": "osm_natural_gen1",
        "fields": "",
        "min_zoom": 9,
        "max_zoom": 10
    },
    {
        "layer_name": "natural_name",
        "table": "osm_natural_gen1",
        "fields": "name",
        "min_zoom": 9,
        "max_zoom": 10,
        "geometry_operator": "point_on_surface",
        "additional_where": f"and name <> ''"
    },
    {
        "layer_name": "natural",
        "table": "osm_natural_gen2",
        "fields": "",
        "min_zoom": 11,
        "max_zoom": 12
    },
        {
        "layer_name": "natural_name",
        "table": "osm_natural_gen2",
        "fields": "name",
        "min_zoom": 11,
        "max_zoom": 12,
        "geometry_operator": "point_on_surface",
        "additional_where": f"and name <> ''"
    },
    {
        "layer_name": "natural",
        "table": "osm_natural_gen3",
        "fields": "",
        "min_zoom": 13,
        "max_zoom": 13
    },
        {
        "layer_name": "natural_name",
        "table": "osm_natural_gen3",
        "fields": "name",
        "min_zoom": 13,
        "max_zoom": 13,
        "geometry_operator": "point_on_surface",
        "additional_where": f"and name <> ''"
    },
    {
        "layer_name": "natural",
        "table": "osm_natural_poly",
        "fields": "",
        "min_zoom": 14
    },
    {
        "layer_name": "natural_name",
        "table": "osm_natural_poly",
        "fields": "name",
        "min_zoom": 14,
        "geometry_operator": "point_on_surface",
        "additional_where": f"and name <> ''"
    },
    {
        "layer_name": "natural_water",
        "table": "osm_natural_water_gen0",
        "fields": "",
        "min_zoom": 6,
        "max_zoom": 8
    },
    {
        "layer_name": "natural_water",
        "table": "osm_natural_water_gen1",
        "fields": "",
        "min_zoom": 9,
        "max_zoom": 10
    },
    {
        "layer_name": "natural_water",
        "table": "osm_natural_water_gen2",
        "fields": "",
        "min_zoom": 11,
        "max_zoom": 12
    },
    {
        "layer_name": "natural_water",
        "table": "osm_natural_water_poly",
        "fields": "",
        "min_zoom": 13
    },
    {
        "layer_name": "waterways",
        "table": "osm_waterways_gen0",
        "fields": "name",
        "min_zoom": 11,
        "max_zoom": 11
    },
    {
        "layer_name": "waterways",
        "table": "osm_waterways_gen1",
        "fields": "name",
        "min_zoom": 12,
        "max_zoom": 12
    },
    {
        "layer_name": "waterways",
        "table": "osm_waterways_way",
        "fields": "name",
        "min_zoom": 13
    },
    {
        "layer_name": "landuse_cemetery",
        "table": "osm_cemeteries_gen0",
        "fields": "name",
        "min_zoom": 11,
        "max_zoom": 11
    },
    {
        "layer_name": "landuse_cemetery",
        "table": "osm_cemeteries_gen1",
        "fields": "name",
        "min_zoom": 12,
        "max_zoom": 12
    },
    {
        "layer_name": "landuse_cemetery",
        "table": "osm_cemeteries_poly",
        "fields": "name",
        "min_zoom": 13
    },
    {
        "layer_name": "landuse_grass",
        "table": "osm_landuse_grass_gen0",
        "fields": "",
        "min_zoom": 11,
        "max_zoom": 11
    },
    {
        "layer_name": "landuse_grass",
        "table": "osm_landuse_grass_gen1",
        "fields": "",
        "min_zoom": 12,
        "max_zoom": 13
    },
    {
        "layer_name": "landuse_grass",
        "table": "osm_landuse_grass_gen2",
        "fields": "",
        "min_zoom": 14,
        "max_zoom": 14
    },
    {
        "layer_name": "landuse_grass",
        "table": "osm_landuse_grass_poly",
        "fields": "",
        "min_zoom": 15
    },
    {
        "layer_name": "towns",
        "table": "osm_place_point",
        "fields": "name, CASE "
                  "WHEN capital THEN 'tgt' "
                  "ELSE  't' "
                  "END as t",
        "min_zoom": 6,
        "max_zoom": 7,
        "additional_where": f"and (type = 'town' or type = 'city' and name = 'London') and population > 40000"
    },
    {
        "layer_name": "towns",
        "table": "osm_place_point",
        "fields": "name, CASE "
                  "WHEN capital THEN 'tgt' "
                  "WHEN type = 'city' THEN 'c' "
                  "ELSE  't' "
                  "END as t ",
        "min_zoom": 8,
        "max_zoom": 8,
        "additional_where": f"and (type = 'town' and population > 20000 or type = 'city' and name = 'London')  and population > 10000"
    },
    {
        "layer_name": "towns",
        "table": "osm_place_point",
        "fields": "name, CASE "
                  "WHEN capital THEN 'tgt' "
                  "WHEN type = 'city' THEN 'c' "
                  "ELSE  't' "
                  "END as t ",
        "min_zoom": 9,
        "max_zoom": 9,
        "additional_where": f"and (type = 'town' or type = 'city' and name = 'London')"
    },
    {
        "layer_name": "towns",
        "table": "osm_place_point",
        "fields": "name, CASE "
                  "WHEN capital THEN 'tgt' "
                  "WHEN type = 'city' THEN 'c' "
                  "WHEN type = 'town' THEN 't' "
                  "ELSE  'v' "
                  "END as t ",
        "min_zoom": 10,
        "max_zoom": 10,
        "additional_where": f"and (type <> 'city' or type = 'city' and name = 'London')"
    },
    {
        "layer_name": "towns",
        "table": "osm_place_point",
        "fields": "name, CASE "
                  "WHEN type = 'village' THEN 'v' "
                  "WHEN type = 'hamlet' THEN 'h' "
                  "ELSE  'd' "
                  "END as t ",
        "min_zoom": 11,
        "max_zoom": 11,
        "additional_where": "and type not in ('city', 'town') and name <> 'London'"
    },
    {
        "layer_name": "admin_names",
        "table": "osm_admin_level_poly",
        "fields": "REPLACE(REPLACE(UPPER(name), 'ROYAL BOROUGH OF ', ''), 'LONDON BOROUGH OF ', '') as name, id",
        "min_zoom": 10,
        "geometry_operator": "centroid",
        "additional_where": f"and ((admin_level = 6 and name = 'City of London') or (admin_level = 8))"
    },
    {
        "layer_name": "admin_borders",
        "table": "osm_admin_level_poly",
        "fields": "id",
        "min_zoom": 8,
        "additional_where": f"and ((admin_level = 6 and name = 'City of London') or (admin_level = 8))"
    },
    {
        "layer_name": "district_names",
        "table": "osm_suburb_point",
        "fields": "UPPER(name) as name, id",
        "min_zoom": 12,
        "max_zoom": 13
    },
    {
        "layer_name": "subway_stations",
        "table": "osm_subway_stations",
        "fields": "city",
        "min_zoom": 11,
        "max_zoom": 11,
        "additional_where": "and railway = 'station'",
    },
    {
        "layer_name": "subway_stations",
        "table": "osm_subway_stations",
        "fields": "name, city",
        "min_zoom": 12,
        "max_zoom": 12,
        "additional_where": "and railway = 'station'"
    },
    {
        "layer_name": "subway_stations",
        "table": "osm_subway_stations",
        "fields": "name, city",
        "min_zoom": 13,
        "additional_where": "and railway = 'station'"
    },
    {
        "layer_name": "subway_entrances",
        "table": "osm_subway_station_entrances",
        "fields": "city",
        "min_zoom": 15
    },
    {
        "layer_name": "airports",
        "table": "ecology_poi",
        "fields": "ref",
        "min_zoom": 8,
        "max_zoom": 10,
        "additional_where": "and poi_type = 'airport' and poi_subtype = 'international'"
    },
    {
        "layer_name": "airports",
        "table": "ecology_poi",
        "fields": "ref",
        "min_zoom": 11,
        "max_zoom": 12,
        "additional_where": "and poi_type = 'airport' and poi_subtype in ('international', 'continental', 'public')"
    },
    {
        "layer_name": "railway_stations",
        "table": "ecology_poi",
        "fields": "'' as title",
        "min_zoom": 9,
        "max_zoom": 11,
        "additional_where": f"and poi_type = 'railway_station' and title in ({railway_stations})"
    },
    {
        "layer_name": "railway_stations",
        "table": "ecology_poi",
        "fields": "REPLACE(title, 'London ', '') as title",
        "min_zoom": 12,
        "additional_where": f"and poi_type = 'railway_station' and title in ({railway_stations})"
    },
    {
        "layer_name": "airports",
        "table": "ecology_poi",
        "fields": "ref",
        "min_zoom": 13,
        "additional_where": "and poi_type = 'airport'"
    }
]

# {
#    "layer_name": "entrance",
#    "table": "osm_entrance_point",
#    "fields": "id, type",
#    "min_zoom": 15
# },

# "station_point": {
#    "table": "osm_station_point",
#    "fields": "id, type, name",
#    "min_zoom": 15
# },
# "parkings": {
#    "table": "osm_amenity_poly",
#    "fields": "id",
#    "min_zoom": 15,
#    "additional_where": "and type = \'parking\' or type =\'parking_space\'"
# },
# "playgrounds": {
#    "table": "osm_leisure_poly",
#    "fields": "id",
#    "min_zoom": 15,
#    "additional_where": "and type = \'playground\'"
# },
