import pipeline.stages.create_infrastructure_coverage.create_infrastructure_coverage as inf_cov
from pipeline.stages.create_infrastructure_coverage.fill_infrastructure_tables import create_infrastructure_pois


def run_stage(database_url: str, navigation_url: str):
    create_infrastructure_pois(database_url)
    inf_cov.create_coverage(database_url, navigation_url)
