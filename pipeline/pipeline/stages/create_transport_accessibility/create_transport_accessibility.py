import pipeline.stages.create_infrastructure_coverage.useful as u
import json
import math
import time

import requests
import psycopg2


PLACES = [
    [51.50449, -0.08647], # The Shard
    [51.51293, -0.12213], # Covent Garden Theatre
    [51.49942, -0.16334], # Harrods
    [51.51125, -0.08692], # Monument (subway station in London City)
    [51.51599, -0.17490]  # Paddington (near Hyde Park)
]

MAX_MINUTES = 60*5

def generalize_hex_tables(conn):
    cursor_s = conn.cursor()

    for suffix in ["_s", "_m", "_l", "_xl"]:
        gen_table = "hex_tiles" + suffix
        print(f"Started generalizing {gen_table} table")
        cursor = conn.cursor(name=f'hex_iterator_{gen_table}')
        cursor.itersize = 500
        cursor.execute(f"SELECT gid, ST_AsText(geometry) FROM {gen_table}")

        for i, row in enumerate(cursor.fetchall()):
            gid, geom = row

            cursor_s.execute(f" SELECT transport_accessibility,"
                             f" ST_Area(ST_Intersection(geometry, 'SRID=3857;{geom}')), ST_Area(geometry)"
                             f" FROM hex_tiles WHERE"
                             f" ST_Intersects(geometry, 'SRID=3857;{geom}')")

            area_sum = 0.0
            transport_accessibility_avg = 0.0

            for row_i in cursor_s.fetchall():
                transport_accessibility, area_inter, area_total = row_i

                transport_accessibility_avg += transport_accessibility * area_inter
                area_sum += area_total
                

            transport_accessibility_avg /= area_sum

            if i % 1000 == 0:
                print(f"Generalizing transport accessibility for {gen_table}, {i}")

            cursor_s.execute(f"UPDATE {gen_table} SET transport_accessibility = {transport_accessibility_avg}"
                             f" WHERE gid = {gid}")

        conn.commit()


def extract_coord_from_text(text):
    try:
        i_start = text.find('(')
        i_end = text.find(')')
        coord = text[i_start + 1:i_end]
        x, y = coord.split(' ')
        return float(x), float(y)
    except Exception as e:
        print("Exception in extract_coord_from_text():", e, text)
    return None, None

def get_eta_minutes(json_response):
    if "paths" not in json_response:
        return MAX_MINUTES
    
    for route in json_response["paths"]:
        if "time" not in route:
            print("No time in route")
            return MAX_MINUTES
        time_ms = route["time"]
        
        return time_ms / (1000.0*60.0)
    
    return MAX_MINUTES


def calc_eta(lat, lon, transit_endpoint):
    avg_eta_minutes = 0.0

    for place in PLACES:
        # https://github.com/graphhopper/graphhopper/blob/master/docs/web/api-doc.md
        payload = {
            'type': 'json', 
            'locale': 'en-US',
            'key': '',
            'pt.earliest_departure_time': '2022-02-05T17:34:00.000Z',
            'pt.limit_solutions': 1,
            'elevation': False,
            'instructions': False,
            'calc_points': False,
            'profile': 'pt',
            'point': [','.join([str(lat), str(lon)]), ','.join([str(place[0]), str(place[1])])]
        }

        while True:
            try:
                resp = requests.get(transit_endpoint, params=payload)

                eta_minutes = get_eta_minutes(resp.json())
                avg_eta_minutes += eta_minutes
                break
            except Exception as e:
                print(f"Exception in calc_eta: {e}.")
                time.sleep(30)
    
    return avg_eta_minutes/len(PLACES)
    


def calc_for_hexes(conn, transit_endpoint, nav_endpoint):
    cursor = conn.cursor(name='hex_transport_iterator')
    cursor.itersize = 300
    cursor_upd = conn.cursor()

    cursor.execute(f" SELECT"
                   f" gid, ST_AsText(ST_Transform(ST_Centroid(geometry), 4326)) "
                   f" FROM hex_tiles")

    print("Fetching hexes geometry: retrieved the cursor")

    for i, rec in enumerate(cursor.fetchall()):
        gid, center_text = rec
        lon, lat = extract_coord_from_text(center_text)
        if lon is None or lat is None:
            continue
        
        time_minutes = calc_eta(lat, lon, transit_endpoint)
        print("Got avg ETA: ", time_minutes)
        cursor_upd.execute(f"UPDATE hex_tiles"
                    f" SET transport_accessibility = {time_minutes}"
                    f" WHERE gid = {gid}")
        
        if i % 10000 == 0:
            print(f"Calculating transport accessibility for hex, {i}")
    
    conn.commit()


def create_transport_accessibility(database_url: str, transit_navigation_url: str, navigation_url: str):
    print("Started calculating transport accessibility")
    conn = psycopg2.connect(database_url)
    print("Connected to db")
    print("Transit url:", transit_navigation_url)

    tic = time.perf_counter()

    calc_for_hexes(conn, transit_navigation_url + "/route", navigation_url + "/sources_to_targets")
    print("Calculated transport accessibility for buildings.")

    generalize_hex_tables(conn)
    print("Generalized transport accessibility for hexes.")

    conn.close()

    toc = time.perf_counter()
    print(f"Finished calculating transport accessibility in {toc - tic:0.1f} s")
