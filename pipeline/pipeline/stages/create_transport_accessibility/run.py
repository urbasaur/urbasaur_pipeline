from pipeline.stages.create_transport_accessibility.create_transport_accessibility import create_transport_accessibility


def run_stage(database_url: str, transit_navigation_url: str, navigation_url: str):
    create_transport_accessibility(database_url, transit_navigation_url, navigation_url)
