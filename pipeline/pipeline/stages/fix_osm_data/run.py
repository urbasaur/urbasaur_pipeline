from pathlib import Path

import pipeline.stages.fix_osm_data.fix_buildings as bld_parts
from pipeline.stages.fix_osm_data.fill_ecology_table import fill_ecology_poi_table
from pipeline.stages.fix_osm_data.fix_water import fix_water
from pipeline.stages.fix_osm_data.prepare_subways import prepare_subways


def run_stage(database_url: str, osm_dir: Path):
    fix_water(database_url)
    prepare_subways(database_url, osm_dir)
    fill_ecology_poi_table(database_url)
    bld_parts.fix_buildings(database_url)
