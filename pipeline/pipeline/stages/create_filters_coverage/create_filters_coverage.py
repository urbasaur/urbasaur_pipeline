import time
from itertools import chain
from pathlib import Path
from typing import List

import requests
import psycopg2

import pyproj as pyproj
from shapely.geometry import Polygon, Point, LineString, MultiLineString
from shapely import wkt
from shapely.ops import transform

from pipeline.services.utils import get_available_cities
from pipeline.services.entities import City


TIME_INTERVALS_DESC = [20, 15, 10, 5]
HEX_TABLE = "hex_tiles"
HEX_SUFFIXES = ["", "_s", "_m", "_l", "_xl"]

def get_isochrones(navigation_url: str, point: Point):
    """Uses isocrhone request to valhalla API:
    https://valhalla.readthedocs.io/en/latest/api/isochrone/api-reference/
    Example of locations: [{"lat":40.744014,"lon":-73.990508}]
    """

    json_val = {
        "locations":[{"lat": point.y, "lon": point.x}],
        "costing":"pedestrian",
        "contours":[],
        "polygons": True
    }

    for t in TIME_INTERVALS_DESC:
        json_val["contours"].append({"time": t})

    resp = requests.post(navigation_url, json=json_val)
    return resp.json()


def parse_polygons_from_isochrones(iso_json, time_to_polygons):
    if "features" not in iso_json or "type" not in iso_json:
        print(f"Error from valhalla: {iso_json}")
        return
    
    for feature in iso_json["features"]:
        max_time_minutes = feature["properties"]["contour"]
        polygon_wgs84 = Polygon(list(chain(*feature["geometry"]["coordinates"])))
        if max_time_minutes not in time_to_polygons:
            time_to_polygons[max_time_minutes] = []

        time_to_polygons[max_time_minutes].append(polygon_wgs84)


def get_points(rows, table, split_to_points=False) -> List[Point]:
    points: List[Point] = []

    for row in rows:
        obj = wkt.loads(row[0])

        if isinstance(obj, Point):
            points.append(obj)
        else:
            if not split_to_points:
                print(f"Geometry from {table}: unsupported type: {type(obj)}. Skipping.")
                continue

            if isinstance(obj, LineString):
                for p in obj.coords:
                    points.append(Point(p[0], p[1]))
            else:
                for l in obj:
                    for p in l.coords:
                        points.append(Point(p[0], p[1]))

    return points


def get_points_inside_polygon(
        table,
        conn,
        polygon_wgs84: Polygon
) -> List[Point]:
    query = f"""
        SELECT ST_AsText(ST_Transform(ST_Centroid(geometry), 4326))
        FROM {table}
        WHERE  ST_Intersects(geometry, ST_Transform('SRID=4326;{polygon_wgs84.wkt}', 3857))
        """
    
    with conn.cursor() as cursor:
        cursor.execute(query)
        return get_points(cursor.fetchall(), table)


def get_park_entrances_inside_polygon( conn, polygon_wgs84: Polygon) -> List[Point]:
    area_m2 = 300*300
    query = f"""
    SELECT ST_AsText(ST_Transform(ST_Intersection(osm_footways_way.geometry, osm_natural_poly.geometry), 4326))
    FROM osm_footways_way, osm_natural_poly
    WHERE ST_Intersects(osm_footways_way.geometry, osm_natural_poly.geometry)
    AND ST_Intersects(osm_natural_poly.geometry, ST_Transform('SRID=4326;{polygon_wgs84.wkt}', 3857))
    AND ST_Area(osm_natural_poly.geometry) > {area_m2}
    """ 

    with conn.cursor() as cursor:
        cursor.execute(query)
        return get_points(cursor.fetchall(), "osm_natural_poly", split_to_points=True)


def get_infrastructure_pois_inside_polygon(
        poi_type,
        conn,
        polygon_wgs84: Polygon
) -> List[Point]:
    table = "infrastructure_poi"
    query = f"""
        SELECT ST_AsText(ST_Transform(geometry, 4326))
        FROM {table}
        WHERE  poi_type = '{poi_type}' and ST_Intersects(geometry, ST_Transform('SRID=4326;{polygon_wgs84.wkt}', 3857))
        """
    
    with conn.cursor() as cursor:
        cursor.execute(query)
        return get_points(cursor.fetchall(), table)


def update_intersecting_polygons(conn, fig, table, field, value):
    with conn.cursor() as cursor:
        cursor.execute(f"""UPDATE {table} SET {field} = {value} WHERE
                        ST_Intersects(geometry, ST_GeomFromText('{fig.wkt}', 3857))""")
        conn.commit()


def update_intersecting_hexes(time_to_isochrones, transformer, conn, field):
    for time_minutes in TIME_INTERVALS_DESC:
        if time_minutes not in time_to_isochrones:
            continue
        
        for isochrone in time_to_isochrones[time_minutes]:
            isochrone_mercator = transform(transformer.transform, isochrone)    

            for suffix in HEX_SUFFIXES:
                update_intersecting_polygons(conn, isochrone_mercator, HEX_TABLE + suffix, field, time_minutes)


def update_field_in_hexes(field, points, conn, transformer, api):
    time_to_isochrones = {}

    for point in points:
        iso_json = get_isochrones(api, point)
        parse_polygons_from_isochrones(iso_json, time_to_isochrones)

    update_intersecting_hexes(time_to_isochrones, transformer, conn, field)


def calc_distance_to_subway_entrances(conn, city: City, transformer, api):
    subway_points = get_points_inside_polygon("osm_subway_station_entrances", conn, city.polygon_wgs84)
    print(f"City {city.name}: {len(subway_points)} subway entrances.")
    update_field_in_hexes("filter_subway", subway_points, conn, transformer, api)


def calc_distance_to_parks(conn, city: City, transformer, api):
    park_entrance_points = get_park_entrances_inside_polygon(conn, city.polygon_wgs84)
    print(f"City {city.name}: {len(park_entrance_points)} entrances to parks.")
    update_field_in_hexes("filter_park", park_entrance_points, conn, transformer, api)


def calc_distances_to_where_to_eat(conn, city, transformer, api):
    eat_points = get_points_inside_polygon("osm_where_to_eat_point", conn, city.polygon_wgs84)
    eat_points += get_points_inside_polygon("osm_where_to_eat_poly", conn, city.polygon_wgs84)
    print(f"City {city.name}: {len(eat_points)} 'where to eat' POIs.")
    update_field_in_hexes("filter_where_to_eat", eat_points, conn, transformer, api)


def calc_distances_schools(conn, city, transformer, api):
    school_points = get_infrastructure_pois_inside_polygon("school", conn, city.polygon_wgs84)
    print(f"City {city.name}: {len(school_points)} schools.")
    update_field_in_hexes("filter_school", school_points, conn, transformer, api)


def calc_distances_kindergartens(conn, city, transformer, api):
    kindergarten_points = get_infrastructure_pois_inside_polygon("kindergarten", conn, city.polygon_wgs84)
    print(f"City {city.name}: {len(kindergarten_points)} kindergartens.")
    update_field_in_hexes("filter_kindergarten", kindergarten_points, conn, transformer, api)


def calc_distances_shops(conn, city, transformer, api):
    shop_points = get_infrastructure_pois_inside_polygon("shop", conn, city.polygon_wgs84)
    print(f"City {city.name}: {len(shop_points)} shops.")
    update_field_in_hexes("filter_shop", shop_points, conn, transformer, api)


def fix_tables_for_tile_seed(conn):
    cursor_upd = conn.cursor()
    cursor_upd.execute("INSERT INTO osm_where_to_eat_point (geometry, name) SELECT ST_Centroid(osm_where_to_eat_poly.geometry), name FROM osm_where_to_eat_poly")
    conn.commit()


def create_coverage(database_url: str, navigation_url: str, work_dir: Path, cities: List[str]):
    print("Started creating filters coverage")

    api = navigation_url + "/isochrone"
    conn = psycopg2.connect(database_url)
    tic = time.perf_counter()

    transformer = pyproj.Transformer.from_crs(pyproj.CRS(4326), pyproj.CRS(3857), always_xy=True)

    available_cities: List[City] = get_available_cities(work_dir, cities)

    for i, city in enumerate(available_cities, start=1):
        print(f"Creating filters for city {city.name}: {i} / {len(available_cities)}")
        calc_distance_to_subway_entrances(conn, city, transformer, api)
        calc_distance_to_parks(conn, city, transformer, api)
        calc_distances_to_where_to_eat(conn, city, transformer, api)
        calc_distances_schools(conn, city, transformer, api)
        calc_distances_shops(conn, city, transformer, api)
        calc_distances_kindergartens(conn, city, transformer, api)

    fix_tables_for_tile_seed(conn)
    conn.close()

    toc = time.perf_counter()
    print(f"Finished creating filters coverage in {toc - tic:0.1f} s")