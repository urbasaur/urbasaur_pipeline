from dataclasses import dataclass
from enum import Enum
from typing import Dict
from typing import List
from typing import Optional

from shapely.geometry import Point
from shapely.geometry import Polygon
from shapely.strtree import STRtree

class HexagonSize(Enum):
    base = 0
    s = 1
    m = 2
    l = 3
    xl = 4
    xxl = 5
    xxxl = 6
    xxxxl = 7
    xxxxxl = 8


@dataclass
class Bounds:
    left: float
    bottom: float
    right: float
    top: float


@dataclass
class GridCell:
    impact: float
    center: Point
    bounds: Bounds
    polygon: Polygon


@dataclass
class ImpactGrid:
    # shapely-objects are unhashable so we use id for keys
    # https://shapely.readthedocs.io/en/stable/manual.html#str-packed-r-tree
    cells: List[GridCell]
    str_tree_cells_inds: Dict[int, int]
    str_tree: STRtree


@dataclass
class Sector:
    scale_factor: float
    polygon: Polygon
    polygon_wgs84: Polygon
    ecology_polygon_wgs84: Polygon


@dataclass
class Settlement:
    polygon: Polygon
    city_type: str
    city_type_impact: float
    population: Optional[int]
    population_impact: Optional[float]


@dataclass
class City:
    id: int
    name: str
    scale_factor: float
    polygon: Polygon
    polygon_wgs84: Polygon


@dataclass
class EcologyHexagon:
    id: int
    impact: float
    polygon_web_mercator: Polygon
    hexagon_size: HexagonSize