# -*- coding: utf-8 -*-

import argparse
import os
import json

geofabrik_url = "http://download.geofabrik.de"


def run_cmd(cmd_str):
    ret_res = os.system(cmd_str)
    if ret_res != 0:
        print(f"Error {ret_res} running {cmd_str}")
        exit()


def get_cities_list(s):
    cities = s.lower().strip().split(',')
    if len(cities) == 1 and cities[0] == "all":
        return []


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-d', '--dir', required=True,
                        help='Output directory for downloaded files.')

    parser.add_argument('-c', '--cities', required=False, default="",
                        help="List of cities separated by commas.")

    args = parser.parse_args()
    cities = get_cities_list(args.cities)

    with open(os.path.join(args.dir, 'supported_cities.json')) as json_file:
        supported_cities = json.load(json_file)

    files_for_del = set()

    for city in supported_cities["cities"]:
        name = city["title_en"]

        if len(cities) > 0 and name not in cities:
            print("Skipping", name)
            continue

        print("Handling", name)
        osm_file_full = city["osm_extract_file"]
        geojson_clip_area = name + "_borders_for_osmium_extract.geojson"
        osm_file_clipped = name + ".osm.pbf"

        path_file = os.path.join(args.dir, osm_file_full)
        path_geojson = os.path.join(args.dir, geojson_clip_area)
        path_file_clipped = os.path.join(args.dir, osm_file_clipped)

        if not os.path.exists(path_file):
            print(f"Downloading {city['osm_extract_file']} from geofabrik")
            run_cmd(f"wget {geofabrik_url}/{city['geofabrik_path']}/{city['osm_extract_file']} -P {args.dir}")


        print(f"Extracting {osm_file_clipped}")
        run_cmd(f"osmium extract -p {path_geojson} {path_file} -o {path_file_clipped}")
        files_for_del.add(path_file)

    print("Finished extracting cities. Deleting huge osm.pbf files.")
    for file in files_for_del:
        os.remove(file)
        print(f"{file} is deleted")
    
    print("Finished preparing osm data.")


if __name__ == '__main__':
    main()
