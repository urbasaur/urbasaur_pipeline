import json
from shapely.geometry import shape, Point
import requests
import time
import argparse
import concurrent.futures
import os
import io
import zipfile
import shutil

BORDERS_GEOJSON_FILE = 'borders_for_london.geojson'
TRANSITLAND_API_KEY = 'Zm7uB9B8ZiQsdw5Y7rM2p58IDsxco6FT'
MAX_RETRIES = 3
SLEEP_TIMEOUT_S = 30
THREADS = 4
FEEDS_PATH = "/feeds/feeds" #os.getenv('FEEDS_DIR')
URLS_FILE = os.path.join(FEEDS_PATH, 'london_feeds.csv')
COMBINED_FEED_PATH = os.path.join(FEEDS_PATH, "combined_london_gtfs_feed")
COMBINED_FEED_ZIP = os.path.join(FEEDS_PATH, "london_gtfs")

GTFS_FIELDS = {
    "agency.txt": ["agency_id", "agency_name", "agency_url", "agency_timezone"],
    "stops.txt": ["stop_id", "stop_code", "stop_name", "stop_lat", "stop_lon", "zone_id", "location_type", "parent_station"],
    "routes.txt": ["route_id", "agency_id", "route_short_name", "route_long_name", "route_type", "route_color"],
    "trips.txt": ["route_id", "service_id", "trip_id", "shape_id"],
    "stop_times.txt": ["trip_id", "arrival_time", "departure_time", "stop_id", "stop_sequence", "timepoint"],
    "calendar.txt": ["service_id", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday", "start_date", "end_date"],
    "calendar_dates.txt": ["service_id", "date", "exception_type"],
    "shapes.txt": ["shape_id", "shape_pt_lat", "shape_pt_lon", "shape_pt_sequence", "shape_dist_traveled"],
    "frequencies.txt": ["trip_id", "start_time", "end_time", "headway_secs", "exact_times"],
    "transfers.txt": ["from_stop_id", "to_stop_id", "transfer_type", "min_transfer_time"]
}


def write_lines_to_file(filepath, lines):
    """Saves list of lines to the filepath."""

    with open(filepath, "w") as out:
        for k, v in lines.items():
            out.write(f"{k}\t{v}\n")
            print(f"\t{k}\t{v}")


def is_feed_status_valid(feed):
    try:
        return feed["feed_state"]["feed_version"]["feed_version_gtfs_import"]["success"]
    except:
        pass
    
    return True


def get_feeds_links(data):
    """Extracts feed urls from the GTFS json description."""
    gtfs_feeds_urls = {}

    for feed in data:
        try:
            if feed["spec"] != "gtfs":
                print(f"Feed {feed['onestop_id']} is not in GTFS: {feed['spec']}")
                continue

            feed_url = ""
            if "urls" in feed and "static_current" in feed["urls"]:
                feed_url = feed["urls"]["static_current"]

            gtfs_feeds_urls[feed["onestop_id"]] = feed_url
        except Exception as e:
            print(f"Feed {feed['onestop_id']} is in invalid format: {e}. Feed: {json.dumps(feed)}")

    print(f"\nFound {len(gtfs_feeds_urls)} feeds on page")

    return gtfs_feeds_urls


def parse_page(url):
    """Parses page with feeds list, extracts feed urls and the next page url."""
    retries = MAX_RETRIES

    while retries > 0:
        try:
            response = requests.get(url)
            response.raise_for_status()

            data = json.loads(response.text)

            if "feeds" in data:
                feed_urls = get_feeds_links(data["feeds"])
            else:
                feed_urls = []

            next_page = ""
            try:
                next_page = data["meta"]["next"]
            except:
                pass

            return feed_urls, next_page

        except requests.exceptions.HTTPError as er:
            print(f"HTTP error {er} downloading zip from {url}")
            if er.response.status_code == 429:
                time.sleep(SLEEP_TIMEOUT_S)
        except requests.exceptions.RequestException as ex:
            print(
                f"Exception {ex} parsing {url}."
            )

        retries -= 1

    return [], ""

def merge_two_dicts(x, y):
    z = x.copy()
    z.update(y)
    return z

def crawl_feed_urls(urls_file):
    """Crawls Transitland API and parses feed urls from json on each page.
    Do not try to parallel it because of the Transitland HTTP requests restriction."""

    start_page = f"https://api.transit.land/api/v2/rest/feeds?apikey={TRANSITLAND_API_KEY}"

    feed_urls = {}
    feed_urls_on_page, next_page = parse_page(start_page)

    while next_page:
        print(f"Loaded {next_page}.")
        feed_urls = merge_two_dicts(feed_urls, feed_urls_on_page)
        feed_urls_on_page, next_page = parse_page(next_page)

    if feed_urls_on_page:
        feed_urls = merge_two_dicts(feed_urls, feed_urls_on_page)

    print("Final list of london feeds:")
    write_lines_to_file(urls_file, feed_urls)
    return len(feed_urls)


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "-w",
        "--work-dir",
        required=True,
        help="Directory for saving file with feed urls, feed zips and final aggragated zip")

    parser.add_argument(
        "-s",
        "--step",
        required=True,
        help="Available steps: all, crawl-urls, load-feeds, filter, combine, zip")

    return parser.parse_args()


def step_crawl_urls():
    print("Started crawling GTFS feed urls")
    feed_count = crawl_feed_urls(URLS_FILE)
    print(f"Crawled {feed_count} feed urls")


def parse_urls_file():
    feed_urls = {}
    with open(URLS_FILE) as f:
        for line in f:
            arr = line.strip().split('\t')
            feed_id = arr[0]
            feed_url = ""
            if len(arr) > 1:
                feed_url = arr[1]
            feed_urls[feed_id] = feed_url
    
    print(f"Read {len(feed_urls)} feed urls from file")
    return feed_urls


def extract_to_path(content, dst_path):
    """Reads content as zip and extracts it to dst_path."""

    try:
        archive = zipfile.ZipFile(io.BytesIO(content))
        archive.extractall(path=dst_path)
        return True
    except zipfile.BadZipfile:
        print("BadZipfile exception.")
    except Exception as e:
        print(f"Exception while unzipping feed: {e}")

    return False


def load_gtfs_feed_zip(path, feed_key):
    """Downloads zip from url and extracts it to path."""
    url = f"https://api.transit.land/api/v2/rest/feeds/{feed_key}/download_latest_feed_version?apikey={TRANSITLAND_API_KEY}"

    try:
        response = requests.get(url, stream=True)
        response.raise_for_status()

        if extract_to_path(response.content, path):
            return True

        print(f"Could not extract zip: {feed_key}")

    except requests.exceptions.HTTPError as http_err:
        print(f"HTTP error {http_err} downloading zip from {url}")
    except requests.exceptions.RequestException as ex:
        print(f"Exception {ex} downloading zip from {url}")

    return False


def load_gtfs_zips_from_urls(dst_dir, feed_id_to_url, threads_count):
    """Concurrently downloads GTFS feed zips from urls to path."""

    err_count = 0

    with concurrent.futures.ProcessPoolExecutor(max_workers=threads_count) as executor:
        future_to_url = {
            executor.submit(
                load_gtfs_feed_zip,
                os.path.join(dst_dir, feed_id),
                feed_id,
            ): feed_id
            for feed_id, feed_url in feed_id_to_url.items()
        }

        for idx_feature, future in enumerate(
            concurrent.futures.as_completed(future_to_url), start=1
        ):
            url = future_to_url[future]

            loaded = future.result()
            if not loaded:
                err_count += 1
            print(f"Handled {idx_feature}/{len(feed_id_to_url)} feed. Loaded = {loaded}. {url}")

    return err_count


def step_load_feeds():
    print("Started loading & filtering GTFS feeds")

    feed_urls = parse_urls_file()
    err_count = load_gtfs_zips_from_urls(FEEDS_PATH, feed_urls, THREADS)

    print(f"Done loading GTFS feeds. {err_count}/{len(feed_urls)} errors.")


def create_with_csv_header(filename, filepath):
    print(f"Creating file {filepath} with csv headers")
    csv_fields = ','.join(GTFS_FIELDS[filename])
    with open(filepath, 'w') as f:
        f.write(f"{csv_fields}\n")

def fill_fields_order(filename, fields):
    fields_order = {}

    for dst_field in GTFS_FIELDS[filename]:
        dst_field_idx = GTFS_FIELDS[filename].index(dst_field)
        src_field_idx = -1
        if dst_field in fields:
            src_field_idx = fields.index(dst_field)
        
        fields_order[dst_field_idx] = src_field_idx

    return fields_order

def append_csv_file(filename, src_filepath, dst_filepath, prefix):
    fields_order = {}

    with open(dst_filepath, 'a') as dst_f:
        with open(src_filepath, 'r') as src_f:
            is_header = True

            for line in src_f:
                fields = line.strip().split(',')
                
                if is_header:
                    is_header = False
                    fields_order = fill_fields_order(filename, fields)
                    continue

                vals = []
                for idx in range(0, len(GTFS_FIELDS[filename])):
                    src_idx = fields_order[idx]
                    if src_idx == -1:
                        vals.append("")
                        continue
                    
                    val = fields[src_idx]
                    if GTFS_FIELDS[filename][idx].endswith("_id"):
                        val = prefix + "_" + val
                    
                    vals.append(val)

                csv_fields = ','.join(vals)
                dst_f.write(f"{csv_fields}\n")


def process_gtfs_file(filename, filepath, combined_path, prefix):
    if filename not in GTFS_FIELDS:
        return
    
    print(f"Handling {filename} in {prefix}")

    combined_file = os.path.join(combined_path, filename)
    if not os.path.exists(combined_file):
        create_with_csv_header(filename, combined_file)
    
    append_csv_file(filename, filepath, combined_file, prefix)


def add_feed_to_combined_data(feed_name, feed_dir, combined_path):
    if not os.path.exists(combined_path):
        os.makedirs(combined_path)
    
    for filename in os.listdir(feed_dir):
        filepath = os.path.join(feed_dir, filename)
        if filepath.endswith(".txt"):
            process_gtfs_file(filename, filepath, combined_path, feed_name)


def get_feed_dirs():
    feeds = {}

    for feed_name in os.listdir(FEEDS_PATH):
        feed_dir = os.path.join(FEEDS_PATH, feed_name)
        if os.path.isdir(feed_dir):
            feeds[feed_name] = feed_dir

    print(f"There are {len(feeds)} dirs in {FEEDS_PATH} with feeds")
    return feeds


def step_combine():
    print("Started combining feeds")
    feeds = get_feed_dirs()
    
    for name, dir in feeds.items():
        print(f"Started parsing feed {name}")
        add_feed_to_combined_data(name, dir, COMBINED_FEED_PATH)
        print(f"Finished parsing feed {name}")
        
    print("Finished combining feeds")


def step_zip():
    print(f"Started zipping {COMBINED_FEED_PATH} to {COMBINED_FEED_ZIP}.zip")
    shutil.make_archive(COMBINED_FEED_ZIP, 'zip', COMBINED_FEED_PATH)
    print("Finished zipping combined feed")


def get_lat_lon_idx(header_fields):
    lat_idx = header_fields.index("stop_lat")
    lon_idx = header_fields.index("stop_lon")
    return lat_idx, lon_idx

def is_feed_in_borders(feed_dir, borders):
    filename = "stops.txt"
    lat_idx = 0
    lon_idx = 0

    stops_path = os.path.join(feed_dir, filename)
    if not os.path.exists(stops_path):
        return False

    with open(stops_path) as src_f:
        is_header = True

        for line in src_f:
            fields = line.strip().split(',')
            
            if is_header:
                is_header = False
                try:
                    lat_idx, lon_idx = get_lat_lon_idx(fields)
                except Exception as e_parse:
                    print(f"Exception parsing coords in {feed_dir}. {e_parse}")
                    return False
                
                continue

            if lat_idx >= len(fields) or lon_idx >= len(fields):
                continue

            try:
                lat = float(fields[lat_idx].strip('\"'))
                lon = float(fields[lon_idx].strip('\"'))
                point = Point(lon, lat)
                if borders.contains(point):
                    print(f"London borders contain point: lat={lat}, lon={lon}")
                    return True
            except Exception as e:
                pass
                #print(f"Exception handling coords in {feed_dir}: lat={fields[lat_idx]}. lon={fields[lon_idx]}. {e}")
                
    return False


def step_filter():
    print("Started filtering feeds for London")

    with open(BORDERS_GEOJSON_FILE) as f:
        feature = json.load(f)
    
    borders = shape(feature['geometry'])
    print("Parsed shape for filtering feeds by location")
    assert(borders.is_valid)

    feed_dirs = get_feed_dirs()

    london_feeds = []

    for feed_name, feed_dir in feed_dirs.items():
        print(f"Anzlyzing stops locations in {feed_name}")

        in_borders = is_feed_in_borders(feed_dir, borders)
        
        if not in_borders:
            #print(f"Removing {feed_name}")
            shutil.rmtree(feed_dir)
            continue
        
        print(f"Feed {feed_name} is in London")
        london_feeds.append(feed_name)

    print(f"Finished filtering feeds for London. Found {len(london_feeds)} feeds: {london_feeds}")


def main():
    args = parse_args()

    if args.step in ["crawl-urls", "all"]:
        step_crawl_urls()
    
    if args.step in ["load-feeds", "all"]:
        step_load_feeds()

    if args.step in ["filter", "all"]:
        step_filter()

    if args.step in ["combine", "all"]:
        step_combine()
    
    if args.step in ["zip", "all"]:
        step_zip()

    print("Done handling transit")


if __name__ == "__main__":
    main()